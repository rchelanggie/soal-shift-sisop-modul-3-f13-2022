#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <stdint.h>

//  gcc -pthread -o [output] input.c

void makeDir(char *path) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", path, NULL};
		execv("/bin/mkdir", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void move(char *dir, char *file, char *dest) 
{
	char source[100];
    sprintf(source, "%s%s", dir, file);
	
	pid_t child_id;	
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mv", source, dest, NULL};
		execv("/bin/mv", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void zipPass(char *password, char *zip, char *file) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"zip", "--password", password, "-r", zip, file, NULL};
		execv("/bin/zip", argv);
	} else {
		while(wait(&status) > 0);
        
	}
}

void removeDir(char *dir) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"rm", "-r", dir, NULL};
		execv("/bin/rm", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void removeFile(char *file) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"rm", file, NULL};
		execv("/bin/rm", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void *unzip( void *arg )
{
    char **argv = (char **) arg;

	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		execv("/bin/unzip", argv);
	} else {
		while(wait(&status) > 0);
        char *zip = argv[3];
        removeFile(zip);
	}    
}

void *makeTxt( void *arg ) {
    char **argv = (char **) arg;

    FILE *fp = fopen(argv[0],"w");
    if (fp == NULL)
        printf("error opening file\n");
    fprintf(fp, "%s", argv[1]);
    fclose(fp);
}

void *download( void *arg )
{
    char **argv = (char **) arg;

	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		execv("/bin/wget", argv);
	} else {
		while(wait(&status) > 0);
	}    
}

int findPattern(char *str, char *pat) 
{
	return (strstr(str, pat) != NULL);
}

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;

void build_decoding_table() {
 
    decoding_table = malloc(256);
 
    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}

unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length) {
 
    if (decoding_table == NULL) build_decoding_table();
 
    if (input_length % 4 != 0) return NULL;
 
    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;
 
    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;
 
    for (int i = 0, j = 0; i < input_length;) {
 
        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
 
        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);
 
        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }
 
    return decoded_data;
}

void *decode( void *arg ) {
    char **argv = (char **) arg;
    char *dir = argv[0];
    char *txt = argv[1];

    char writePath[100];
    sprintf(writePath, "%s%s", dir, txt);

	FILE *fpw = fopen(writePath, "w");
	if (fpw == NULL)
		printf("error opening file\n");
	DIR *dp;
	struct dirent *dirp;
    dp = opendir(dir);
    while ((dirp = readdir(dp)) != NULL) {
        if (strcmp(dirp->d_name, ".") != 0 && 
            strcmp(dirp->d_name, "..") != 0 && 
            strcmp(dirp->d_name, txt) != 0) 
        {
            char readPath[256];
            sprintf(readPath, "%s%s", dir, dirp->d_name);

            FILE *fpr = fopen(readPath, "r");
            if (fpr == NULL)
                printf("error opening file\n");

            char encoded[256];
            fgets(encoded, sizeof(encoded), fpr);
            fclose(fpr);

            long decode_size = strlen(encoded);
            char *decoded = base64_decode(encoded, decode_size, &decode_size);
	    	fprintf(fpw, "%s\n", decoded);
        }
    }
    fclose(fpw);
    closedir(dp);
}
int main(void)
{
    char *musicLink = "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1";
    char *quoteLink = "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt";
    char *musicZip = "music.zip";
    char *quoteZip = "quote.zip";
    char *hasilZip = "hasil.zip";
    char *musicDir = "music/";
    char *quoteDir = "quote/";
    char *hasilDir = "hasil/";
    char *musicTxt = "music.txt";
    char *quoteTxt = "quote.txt";
    char *password = "mihinomestdoanda";
    char *noTxt = "no.txt";


    pthread_t download_t[2];
    char *dlArgs1[] = {"wget", "--no-check-certificate", musicLink, "-O", musicZip, NULL};
    char *dlArgs2[] = {"wget", "--no-check-certificate", quoteLink, "-O", quoteZip, NULL};
    pthread_create(&(download_t[0]),NULL,&download, (void*) dlArgs1);  
    pthread_create(&(download_t[1]),NULL,&download, (void*) dlArgs2);  
    pthread_join(download_t[0], NULL);
    pthread_join(download_t[1], NULL);

    pthread_t unzip_t[2];
    char *unzipArgs1[] = {"unzip", "-d", musicDir, musicZip, NULL};
    char *unzipArgs2[] = {"unzip", "-d", quoteDir, quoteZip, NULL};
    pthread_create(&(unzip_t[0]),NULL,&unzip, (void*) unzipArgs1);  
    pthread_create(&(unzip_t[1]),NULL,&unzip, (void*) unzipArgs2);  
    pthread_join(unzip_t[0], NULL);
    pthread_join(unzip_t[1], NULL);

    pthread_t decode_t[2];
    char *decodeArgs1[] = {musicDir, musicTxt};
    char *decodeArgs2[] = {quoteDir, quoteTxt};
    pthread_create(&(decode_t[0]),NULL,&decode, (void*) decodeArgs1); 
    pthread_create(&(decode_t[1]),NULL,&decode, (void*) decodeArgs2); 
    pthread_join(decode_t[0], NULL);
    pthread_join(decode_t[1], NULL);    

    makeDir(hasilDir);
    move(musicDir, musicTxt, hasilDir);
    move(quoteDir, quoteTxt, hasilDir);

    removeDir(musicDir);
    removeDir(quoteDir);

    zipPass(password, hasilZip, hasilDir);
    removeDir(hasilDir);

    pthread_t unzipPass_t;
    pthread_t makeTxt_t;
    char *unzipPassArgs[] = {"unzip", "-P", password, hasilZip, NULL}; 
    char *makeTxtArgs[] = {noTxt, "No"};
    pthread_create(&(unzipPass_t),NULL,&unzip, (void*) unzipPassArgs);  
    pthread_create(&(makeTxt_t),NULL,&makeTxt, (void*) makeTxtArgs);  
    pthread_join(unzipPass_t, NULL);
    pthread_join(makeTxt_t, NULL);

    move("", noTxt, hasilDir);
    zipPass(password, hasilZip, hasilDir);
    removeDir(hasilDir);

    // // download
    // system("wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1' -O music.zip");

    // system("wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt' -O quote.zip");

    // system("unzip -d music music.zip");
    // system("unzip -d quote quote.zip");
    
    // // opendir, opentxt
    // // traverse music folder, if not . .. music.txt decode
	// FILE *fp = fopen("music/music.txt", "w");
	// if (fp == NULL)
	// 	printf("error opening file\n");
	// DIR *dp;
	// struct dirent *dirp;
    // dp = opendir("music");
    // while ((dirp = readdir(dp)) != NULL) {
    //     if (strcmp(dirp->d_name, ".") != 0 && 
    //         strcmp(dirp->d_name, "..") != 0 && 
    //         strcmp(dirp->d_name, "music.txt") != 0) {
	//     	fprintf(fp, "%s\n", decode64("music/", dirp->d_name, "music.txt"));
    //     }
    // }
    // fclose(fp);
    // closedir(dp);

    // // opendir, opentxt
    // // traverse music folder, if not . .. music.txt decode
	// fp = fopen("quote/quote.txt", "w");
	// if (fp == NULL)
	// 	printf("error opening file\n");
    // dp = opendir("quote");
    // while ((dirp = readdir(dp)) != NULL) {
    //     if (strcmp(dirp->d_name, ".") != 0 && 
    //         strcmp(dirp->d_name, "..") != 0 && 
    //         strcmp(dirp->d_name, "quote.txt") != 0) {
	//     	fprintf(fp, "%s\n", decode64("quote/", dirp->d_name, "quote.txt"));
    //     }
    // }
    // fclose(fp);
    // closedir(dp);

    // // makeDir, move
    // system("mkdir hasil");
    // system("mv music/music.txt hasil");
    // system("mv quote/quote.txt hasil");

    // // zipPass, removeDir
    // system("zip --password mihinomenestdoanda -r hasil.zip hasil/");
    // system("rm -r hasil/");
    // // zip --password $password -r $folder.zip $folder/

    // // unzip, removeFile
    // system("unzip -P mihinomenestdoanda hasil.zip");
    // system("rm hasil.zip");

    // fp = fopen("hasil/no.txt", "w");
    // fprintf(fp, "No\n");
    // fclose(fp);

    // system("zip --password mihinomenestdoanda -r hasil.zip hasil/");



    exit(EXIT_SUCCESS);
}

