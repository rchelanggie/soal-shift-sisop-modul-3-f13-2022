# soal-shift-sisop-modul-3-F13-2022

## Anggota Kelompok 
1. Shaula Aljauhara Riyadi 5025201265
2. Cindi Dwi Pramudita 5025201042
3. Rachel Anggieuli AP 5025201263

## Soal 1

Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

### A. 
Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

void zipPass(char *password, char *zip, char *file) 
    void *download( void *arg )
    {
    char **argv = (char **) arg;

	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		execv("/bin/wget", argv);
	} else {
		while(wait(&status) > 0);
	}    
    }


    void *unzip( void *arg )
    {
    char **argv = (char **) arg;

	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		execv("/bin/unzip", argv);
	} else {
		while(wait(&status) > 0);
        char *zip = argv[3];
        removeFile(zip);
	}    
    }

### B.

Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.


    void *makeTxt( void *arg ) {
    char **argv = (char **) arg;

    FILE *fp = fopen(argv[0],"w");
    if (fp == NULL)
    printf("error opening file\n");
    fprintf(fp, "%s", argv[1]);
    fclose(fp);
    }

    void build_decoding_table() {
 
    decoding_table = malloc(256);
 
    for (int i = 0; i < 64; i++)
    decoding_table[(unsigned char) encoding_table[i]] = i;
    }

    unsigned char *base64_decode(const char *data,
    size_t input_length,
    size_t *output_length) {
 
    if (decoding_table == NULL) build_decoding_table();
 
    if (input_length % 4 != 0) return NULL;
 
    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;
 
    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;
 
    for (int i = 0, j = 0; i < input_length;) {
 
    uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
    uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
    uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
    uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
 
    uint32_t triple = (sextet_a << 3 * 6)
    +(sextet_b << 2 * 6)
    +(sextet_c << 1 * 6)
    +(sextet_d << 0 * 6);
 
    if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
    if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
    if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }
 
    return decoded_data;
    }

    void *decode( void *arg ) {
    char **argv = (char **) arg;
    char *dir = argv[0];
    char *txt = argv[1];

    char writePath[100];
    sprintf(writePath, "%s%s", dir, txt);

	FILE *fpw = fopen(writePath, "w");
	if (fpw == NULL)
		printf("error opening file\n");
	DIR *dp;
	struct dirent *dirp;
    dp = opendir(dir);
    while ((dirp = readdir(dp)) != NULL) {
    if (strcmp(dirp->d_name, ".") != 0 && 
    strcmp(dirp->d_name, "..") != 0 && 
    strcmp(dirp->d_name, txt) != 0) 
    {
    char readPath[256];
    sprintf(readPath, "%s%s", dir, dirp->d_name);

    FILE *fpr = fopen(readPath, "r");
    if (fpr == NULL)
    printf("error opening file\n");

    char encoded[256];
    fgets(encoded, sizeof(encoded), fpr);
    fclose(fpr);

    long decode_size = strlen(encoded);
    char *decoded = base64_decode(encoded, decode_size, &decode_size);
	fprintf(fpw, "%s\n", decoded);       
    }        
    }
    fclose(fpw);
    closedir(dp);
    }

### C.
Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.
void move(char *dir, char *file, char *dest) 
    {
	char source[100];
    sprintf(source, "%s%s", dir, file);
	
	pid_t child_id;	
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mv", source, dest, NULL};
		execv("/bin/mv", argv);
	} else {
		while(wait(&status) > 0);
	}
    }


### D.
Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

    void zipPass(char *password, char *zip, char *file) 
    {
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"zip", "--password", password, "-r", zip, file, NULL};
		execv("/bin/zip", argv);
	} else {
		while(wait(&status) > 0);
        
	}
    }

![](soal1/hasil1.png)

![](soal1/hasil2.png)
